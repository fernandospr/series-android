package com.fernandospr.series.common

import android.content.Context
import com.fernandospr.series.common.repository.SeriesRepository
import com.fernandospr.series.common.repository.SeriesRepositoryImpl
import com.fernandospr.series.common.repository.SubscriptionRepository
import com.fernandospr.series.common.repository.SubscriptionRepositoryImpl
import com.fernandospr.series.common.repository.network.SeriesApi
import com.fernandospr.series.detail.view.DetailActivity
import com.fernandospr.series.detail.DetailModule
import com.fernandospr.series.main.view.MainActivity
import com.fernandospr.series.main.MainModule
import com.fernandospr.series.search.view.SearchActivity
import com.fernandospr.series.search.SearchModule
import dagger.Component
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(val app: SeriesApplication) {
    @Provides
    @Singleton
    fun provideApp() = app

    @Provides
    @Singleton
    fun provideSeriesApi() = SeriesApi.create()

    @Provides
    @Singleton
    fun provideSeriesRepository(seriesApi: SeriesApi,
                                subscriptionRepository: SubscriptionRepository): SeriesRepository {
        return SeriesRepositoryImpl(seriesApi, subscriptionRepository)
    }

    @Provides
    @Singleton
    fun provideSubscriptionRepository(app: SeriesApplication): SubscriptionRepository {
        return SubscriptionRepositoryImpl(
                app.getSharedPreferences("Subscriptions", Context.MODE_PRIVATE)
        )
    }
}

@Singleton
@Component(
        modules = arrayOf(
                AppModule::class,
                MainModule::class,
                SearchModule::class,
                DetailModule::class
        )
)
interface AppComponent {
    fun inject(activity: MainActivity)
    fun inject(activity: SearchActivity)
    fun inject(activity: DetailActivity)
}