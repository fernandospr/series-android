package com.fernandospr.series.common

abstract class BasePresenter<V> {
    protected var view: V? = null

    open fun attachView(view: V) {
        this.view = view
    }

    open fun detachView() {
        this.view = null
    }
}