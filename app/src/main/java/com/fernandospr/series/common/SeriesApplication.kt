package com.fernandospr.series.common

import android.app.Application

class SeriesApplication : Application() {

    val component: AppComponent by lazy {
        DaggerAppComponent
                .builder()
                .appModule(AppModule(this))
                .build()
    }

}