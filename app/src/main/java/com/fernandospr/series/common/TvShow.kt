package com.fernandospr.series.common

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Main(val subscribedShows: List<TvShow>,
                val suggestedShows: List<TvShow>,
                val suggestedShowsPage: Int = 1,
                val suggestedShowsTotalPages: Int = 1) : Parcelable {
    fun isEmpty(): Boolean {
        return subscribedShows.isEmpty() && suggestedShows.isEmpty()
    }

    fun subscribe(tvShow: TvShow) : Main {
        val newSubscribed = arrayListOf<TvShow>()
        newSubscribed.add(tvShow)
        if (subscribedShows.isNotEmpty()) {
            newSubscribed.addAll(subscribedShows)
        }

        val newSuggested = arrayListOf<TvShow>()
        if (suggestedShows.isNotEmpty()) {
            newSuggested.addAll(suggestedShows)
        }
        val suggested = newSuggested.find { it.id == tvShow.id }
        if (suggested != null) {
            suggested.subscribed = true
        }

        return Main(newSubscribed, newSuggested, suggestedShowsPage, suggestedShowsTotalPages)
    }

    fun unsubscribe(tvShow: TvShow) : Main {
        val newSubscribed = arrayListOf<TvShow>()
        val filteredSubscribed = subscribedShows.filter { it.id != tvShow.id }
        if (filteredSubscribed.isNotEmpty()) {
            newSubscribed.addAll(filteredSubscribed)
        }

        val newSuggested = arrayListOf<TvShow>()
        if (suggestedShows.isNotEmpty()) {
            newSuggested.addAll(suggestedShows)
        }
        val suggested = newSuggested.find { it.id == tvShow.id }
        if (suggested != null) {
            suggested.subscribed = false
        }

        return Main(newSubscribed, newSuggested, suggestedShowsPage, suggestedShowsTotalPages)
    }

}

@Parcelize
data class TvShow(val id: String,
                  val title: String,
                  val genre: String?,
                  val posterUrl: String,
                  val backdropUrl: String,
                  val overview: String?,
                  val year: String?,
                  var subscribed: Boolean = false) : Parcelable