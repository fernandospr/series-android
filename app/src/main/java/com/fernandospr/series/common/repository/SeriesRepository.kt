package com.fernandospr.series.common.repository

import com.fernandospr.series.common.Main
import com.fernandospr.series.common.TvShow

interface SeriesRepository {
    fun getMain(callback: RepositoryCallback<Main>, page: Int = 1, previousMain: Main? = null)
    fun search(query: String, callback: RepositoryCallback<List<TvShow>>, page: Int = 1)

    fun subscribe(tvShow: TvShow)
    fun unsubscribe(tvShow: TvShow)

    fun addSubscriptionListener(listener: SubscriptionRepositoryListener)
    fun removeSubscriptionListener(listener: SubscriptionRepositoryListener)
}

interface RepositoryCallback<in T> {
    fun onSuccess(t: T?)
    fun onError()
}