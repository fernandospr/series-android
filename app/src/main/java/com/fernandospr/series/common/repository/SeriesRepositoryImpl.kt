package com.fernandospr.series.common.repository

import com.fernandospr.series.common.Main
import com.fernandospr.series.common.TvShow
import com.fernandospr.series.common.repository.network.*
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Function3
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers

class SeriesRepositoryImpl(private val api: SeriesApi,
                           private val subscriptionRepository: SubscriptionRepository) : SeriesRepository {

    private val observableConfig = api.getConfiguration().cache()
    private val observableGenres = api.getGenres().cache()

    override fun getMain(callback: RepositoryCallback<Main>, page: Int, previousMain: Main?) {
        val observablePopular = api.getPopular(page)
        Observable.zip(
                observableConfig,
                observableGenres,
                observablePopular,
                Function3<ApiConfigurationContainer, ApiGenreContainer, ApiTvShowsContainer, Main> {
                    t1, t2, t3 -> buildMain(t1, t2, t3, previousMain)
                }
        )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableObserver<Main>() {
                    override fun onComplete() {
                        // no-op
                    }

                    override fun onError(e: Throwable?) {
                        callback.onError()
                    }

                    override fun onNext(value: Main?) {
                        callback.onSuccess(value)
                    }
                })
    }

    override fun search(query: String, callback: RepositoryCallback<List<TvShow>>, page: Int) {
        val observableSearch = api.search(page, query)
        Observable.zip(
                observableConfig,
                observableGenres,
                observableSearch,
                Function3<ApiConfigurationContainer, ApiGenreContainer, ApiTvShowsContainer, List<TvShow>> {
                    t1, t2, t3 -> buildSearchResults(t1, t2, t3)
                }
        )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableObserver<List<TvShow>>() {
                    override fun onComplete() {
                        // no-op
                    }

                    override fun onError(e: Throwable?) {
                        callback.onError()
                    }

                    override fun onNext(value: List<TvShow>?) {
                        callback.onSuccess(value)
                    }
                })
    }

    private fun buildMain(config: ApiConfigurationContainer?,
                          genres: ApiGenreContainer?,
                          apiTvShowsContainer: ApiTvShowsContainer?,
                          previousMain: Main?): Main {
        val subscribedShows = subscriptionRepository.getSubscribedTvShows()
        val suggestedShows = arrayListOf<TvShow>()

        apiTvShowsContainer?.results?.mapNotNullTo(suggestedShows) {
            buildTvShow(config, genres, it)
        }
        updateSubscriptionStatus(subscribedShows)
        updateSubscriptionStatus(suggestedShows)

        return if (previousMain != null) {
            Main(subscribedShows,
                    previousMain.suggestedShows + suggestedShows,
                    apiTvShowsContainer?.page ?: 1,
                    apiTvShowsContainer?.totalPages ?: 1)
        } else {
            Main(subscribedShows,
                    suggestedShows,
                    apiTvShowsContainer?.page ?: 1,
                    apiTvShowsContainer?.totalPages ?: 1)
        }
    }

    private fun buildTvShow(config: ApiConfigurationContainer?,
                            genres: ApiGenreContainer?,
                            apiTvShow: ApiTvShow): TvShow? {
        val id = apiTvShow.id
        val title = apiTvShow.name
        val genreIds = apiTvShow.genreIds
        val posterPath = apiTvShow.posterPath
        val backdropPath = apiTvShow.backdropPath
        if (id == null || id.isBlank()
                || title == null || title.isBlank()
                || genreIds == null || genreIds.isEmpty()
                || posterPath == null || posterPath.isBlank()
                || backdropPath == null || backdropPath.isBlank()) {
            return null
        } else {
            return TvShow(
                    id,
                    title,
                    genres?.getName(genreIds[0]),
                    config?.buildPosterFullPath(posterPath).orEmpty(),
                    config?.buildBackdropFullPath(backdropPath).orEmpty(),
                    apiTvShow.overview,
                    apiTvShow.firstAirDate?.split("-")?.first()
            )
        }
    }

    private fun buildSearchResults(config: ApiConfigurationContainer?,
                                   genres: ApiGenreContainer?,
                                   apiTvShowsContainer: ApiTvShowsContainer?): List<TvShow> {
        val results = arrayListOf<TvShow>()

        apiTvShowsContainer?.results?.mapNotNullTo(results) {
            buildTvShow(config, genres, it)
        }
        updateSubscriptionStatus(results)

        return results
    }

    private fun updateSubscriptionStatus(results: List<TvShow>) {
        val subscribedTvShows = subscriptionRepository.getSubscribedTvShows()
        if (subscribedTvShows.isNotEmpty()) {
            val subscribedTvShowsIds = subscribedTvShows.map { it.id }
            for (item in results) {
                item.subscribed = subscribedTvShowsIds.contains(item.id)
            }
        }
    }

    override fun subscribe(tvShow: TvShow) {
        subscriptionRepository.subscribe(tvShow)
    }

    override fun unsubscribe(tvShow: TvShow) {
        subscriptionRepository.unsubscribe(tvShow)
    }

    override fun addSubscriptionListener(listener: SubscriptionRepositoryListener) {
        subscriptionRepository.addListener(listener)
    }

    override fun removeSubscriptionListener(listener: SubscriptionRepositoryListener) {
        subscriptionRepository.removeListener(listener)
    }
}