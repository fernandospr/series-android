package com.fernandospr.series.common.repository

import com.fernandospr.series.common.TvShow

interface SubscriptionRepository {
    fun subscribe(tvShow: TvShow)
    fun unsubscribe(tvShow: TvShow)
    fun getSubscribedTvShows(): List<TvShow>
    fun addListener(listener: SubscriptionRepositoryListener)
    fun removeListener(listener: SubscriptionRepositoryListener)
}

interface SubscriptionRepositoryListener {
    fun onSubscribed(tvShow: TvShow)
    fun onUnsubscribed(tvShow: TvShow)
}