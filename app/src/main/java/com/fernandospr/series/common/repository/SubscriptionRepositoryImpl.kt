package com.fernandospr.series.common.repository

import android.content.SharedPreferences
import com.fernandospr.series.common.TvShow
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class SubscriptionRepositoryImpl(val sharedPreferences: SharedPreferences) : SubscriptionRepository {

    private var subscriptionListeners = arrayListOf<SubscriptionRepositoryListener>()

    private val gson = Gson()

    companion object {
        const val SUBSCRIBED_KEY = "SUBSCRIBED_KEY"
    }

    override fun subscribe(tvShow: TvShow) {
        tvShow.subscribed = true
        val subscribedShows = arrayListOf(tvShow) + getSubscribedTvShows()
        saveSubscribedTvShows(subscribedShows)

        subscriptionListeners.forEach {
            it.onSubscribed(tvShow)
        }
    }

    override fun unsubscribe(tvShow: TvShow) {
        val favorites = getSubscribedTvShows() - tvShow
        saveSubscribedTvShows(favorites)
        tvShow.subscribed = false

        subscriptionListeners.forEach {
            it.onUnsubscribed(tvShow)
        }
    }

    private fun saveSubscribedTvShows(tvShows: List<TvShow>) {
        val editor = sharedPreferences.edit()
        editor.putString(SUBSCRIBED_KEY, gson.toJson(tvShows))
        editor.apply()
    }

    private inline fun <reified T> Gson.fromJson(json: String): T = this.fromJson<T>(json, object : TypeToken<T>() {}.type)

    override fun getSubscribedTvShows(): List<TvShow> {
        val subsribedString = sharedPreferences.getString(SUBSCRIBED_KEY, null)
        if (subsribedString != null) {
            return gson.fromJson(subsribedString)
        }
        return emptyList()
    }

    override fun addListener(observer: SubscriptionRepositoryListener) {
        subscriptionListeners.add(observer)
    }

    override fun removeListener(observer: SubscriptionRepositoryListener) {
        subscriptionListeners.remove(observer)
    }

}