package com.fernandospr.series.common.repository.network

data class ApiConfigurationContainer(val images: ApiConfigurationImages) {
    fun buildPosterFullPath(path: String) : String {
        return images.secureBaseUrl + "w342" + path
    }

    fun buildBackdropFullPath(path: String): String {
        return images.secureBaseUrl + "w780" + path
    }
}

data class ApiConfigurationImages(val secureBaseUrl: String)