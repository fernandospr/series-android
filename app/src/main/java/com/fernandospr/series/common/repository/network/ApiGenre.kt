package com.fernandospr.series.common.repository.network

data class ApiGenreContainer(val genres: List<ApiGenre>) {
    fun getName(id: Long): String? {
        for (genre in genres) {
            if (genre.id == id) {
                return genre.name
            }
        }
        return null
    }
}

data class ApiGenre(val id: Long,
                    val name: String)