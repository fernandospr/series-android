package com.fernandospr.series.common.repository.network

data class ApiTvShowsContainer(val page: Int,
                               val totalResults: Int,
                               val totalPages: Int,
                               val results: List<ApiTvShow>)

data class ApiTvShow(val id: String?,
                     val name: String?,
                     val posterPath: String?,
                     val genreIds: List<Long>?,
                     val backdropPath: String?,
                     val overview: String?,
                     val firstAirDate: String?)