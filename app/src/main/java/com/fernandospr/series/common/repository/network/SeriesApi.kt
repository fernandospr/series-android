package com.fernandospr.series.common.repository.network

import com.fernandospr.series.BuildConfig
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import io.reactivex.Observable
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.concurrent.TimeUnit


interface SeriesApi {

    // https://developers.themoviedb.org/3/configuration/get-api-configuration
    @GET("configuration")
    fun getConfiguration(): Observable<ApiConfigurationContainer>

    // https://developers.themoviedb.org/3/genres/get-tv-list
    @GET("genre/tv/list")
    fun getGenres(): Observable<ApiGenreContainer>

    // https://developers.themoviedb.org/3/search/search-tv-shows
    @GET("search/tv")
    fun search(@Query("page") page: Int, @Query("query") query: String): Observable<ApiTvShowsContainer>

    // https://developers.themoviedb.org/3/tv/get-popular-tv-shows
    @GET("tv/popular")
    fun getPopular(@Query("page") page: Int): Observable<ApiTvShowsContainer>

    companion object Factory {

        fun create(): SeriesApi {
            val httpClient = getHttpClient()

            val gsonConverterFactory = getGsonConverterFactory()

            val retrofit = Retrofit.Builder()
                    .baseUrl(BuildConfig.API_BASE_URL)
                    .addConverterFactory(gsonConverterFactory)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(httpClient)
                    .build()

            return retrofit.create(SeriesApi::class.java)
        }

        private fun getHttpClient(): OkHttpClient {
            val httpClient = OkHttpClient.Builder()
                    .connectTimeout(10, TimeUnit.SECONDS)
                    .writeTimeout(10, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS)

            httpClient
                    .addInterceptor(getLoggingInterceptor())
                    .addInterceptor(getApiKeyInterceptor())

            return httpClient.build()
        }

        private fun getLoggingInterceptor(): Interceptor {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = BuildConfig.RETROFIT_LOGGING
            return interceptor
        }

        private fun getApiKeyInterceptor(): Interceptor = Interceptor { chain ->
            val original = chain.request()
            val originalHttpUrl = original.url()

            val url = originalHttpUrl.newBuilder()
                    .addQueryParameter("api_key", BuildConfig.API_KEY)
                    .build()

            val request = original.newBuilder().url(url).build()
            chain.proceed(request)
        }

        private fun getGsonConverterFactory() : Converter.Factory {
            val gson = GsonBuilder()
                    .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                    .create()

            return GsonConverterFactory.create(gson)
        }
    }
}