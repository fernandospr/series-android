package com.fernandospr.series.common.view

import android.support.v7.app.AppCompatActivity
import android.view.View
import com.fernandospr.series.R
import kotlinx.android.synthetic.main.view_error.*
import kotlinx.android.synthetic.main.view_loading.*
import kotlinx.android.synthetic.main.view_noresults.*

abstract class BaseActivity : AppCompatActivity() {
    fun showLoadingContainer() {
        loadingContainer.visibility = View.VISIBLE
        noresultsContainer.visibility = View.GONE
        errorContainer.visibility = View.GONE
        getContentContainer().visibility = View.GONE
    }

    fun showErrorContainer(retryCallback: (() -> Unit)? = null) {
        loadingContainer.visibility = View.GONE
        noresultsContainer.visibility = View.GONE
        errorContainer.visibility = View.VISIBLE
        getContentContainer().visibility = View.GONE
        if (retryCallback != null) {
            retryButton.setOnClickListener {
                retryCallback()
            }
            retryButton.visibility = View.VISIBLE
        } else {
            retryButton.visibility = View.GONE
        }
    }

    fun showNoResultsContainer(noresultsText: String = getString(R.string.main_noresults)) {
        loadingContainer.visibility = View.GONE
        noresultsContainer.visibility = View.VISIBLE
        errorContainer.visibility = View.GONE
        getContentContainer().visibility = View.GONE
        noresultsTitle.text = noresultsText
    }

    fun showContentContainer() {
        loadingContainer.visibility = View.GONE
        noresultsContainer.visibility = View.GONE
        errorContainer.visibility = View.GONE
        getContentContainer().visibility = View.VISIBLE
    }

    abstract fun getContentContainer() : View
}