package com.fernandospr.series.detail

import com.fernandospr.series.common.repository.SubscriptionRepository
import dagger.Module
import dagger.Provides

@Module
class DetailModule {

    @Provides
    fun provideDetailPresenter(subscriptionRepository: SubscriptionRepository) = DetailPresenter(subscriptionRepository)

}
