package com.fernandospr.series.detail

import com.fernandospr.series.common.BasePresenter
import com.fernandospr.series.common.TvShow
import com.fernandospr.series.common.repository.SubscriptionRepository

class DetailPresenter(private val repository: SubscriptionRepository) :
        BasePresenter<DetailPresenter.View>() {

    private lateinit var tvShow : TvShow

    fun init(tvShow: TvShow) {
        this.tvShow = tvShow
        view?.showDetail(tvShow)
    }

    fun toggleSubscription() {
        if (tvShow.subscribed) {
            repository.unsubscribe(tvShow)
        } else {
            repository.subscribe(tvShow)
        }
        view?.refreshSubscriptionStatus(tvShow)
    }

    interface View {
        fun showDetail(tvShow: TvShow)
        fun refreshSubscriptionStatus(tvShow: TvShow)
    }
}