package com.fernandospr.series.detail.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.fernandospr.series.R
import com.fernandospr.series.common.SeriesApplication
import com.fernandospr.series.common.TvShow
import com.fernandospr.series.detail.DetailPresenter
import com.github.florent37.glidepalette.BitmapPalette
import com.github.florent37.glidepalette.GlidePalette
import kotlinx.android.synthetic.main.activity_detail.*
import javax.inject.Inject


class DetailActivity : AppCompatActivity(), DetailPresenter.View {

    @Inject lateinit var presenter: DetailPresenter

    companion object {
        private const val EXTRA_SHOW = "EXTRA_SHOW"

        fun newIntent(context: Context, tvShow: TvShow): Intent {
            val intent = Intent(context, DetailActivity::class.java)
            intent.putExtra(EXTRA_SHOW, tvShow)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        (application as SeriesApplication).component.inject(this)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val tvShow = intent.getParcelableExtra<TvShow>(EXTRA_SHOW)
        title = null

        presenter.attachView(this)
        presenter.init(tvShow)
    }

    override fun onSupportNavigateUp(): Boolean {
        ActivityCompat.finishAfterTransition(this)
        return true
    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }

    override fun showDetail(tvShow: TvShow) {
        overviewText.text = tvShow.overview
        titleText.text = tvShow.title
        yearText.text = tvShow.year
        refreshSubscriptionStatus(tvShow)
        Glide.with(this).load(tvShow.posterUrl)
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(imageView)
        Glide.with(this).load(tvShow.backdropUrl)
                .listener(
                        GlidePalette.with(tvShow.backdropUrl)
                                .use(BitmapPalette.Profile.MUTED)
                                .intoBackground(backdropOverlay)
                                .crossfade(true)
                )
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(backdropImageView)
    }

    override fun refreshSubscriptionStatus(tvShow: TvShow) {
        setupSubscribeButton(tvShow.subscribed)
    }

    private fun setupSubscribeButton(subscribed: Boolean) {
        if (subscribed) {
            subscribeButton.setTextColor(ContextCompat.getColor(this, android.R.color.black))
            subscribeButton.text = getString(R.string.subscribed)
            subscribeButton.setBackgroundResource(R.drawable.bg_subscribed)
        } else {
            subscribeButton.setTextColor(ContextCompat.getColor(this, android.R.color.white))
            subscribeButton.text = getString(R.string.subscribe)
            subscribeButton.setBackgroundResource(R.drawable.bg_subscribe)
        }
        subscribeButton.setOnClickListener {
            presenter.toggleSubscription()
        }
    }

}
