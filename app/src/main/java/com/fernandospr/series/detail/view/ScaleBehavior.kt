package com.fernandospr.series.detail.view

import android.content.Context
import android.support.design.widget.AppBarLayout
import android.support.design.widget.CoordinatorLayout
import android.util.AttributeSet
import android.view.View
import com.fernandospr.series.R

class ScaleBehavior<V : View>(context: Context, attrs: AttributeSet)
    : CoordinatorLayout.Behavior<V>(context, attrs) {

    companion object {
        private val MIN_SCALE = 0.15f
    }

    override fun layoutDependsOn(parent: CoordinatorLayout, child: V, dependency: View): Boolean {
        return dependency is AppBarLayout
    }

    override fun onDependentViewChanged(parent: CoordinatorLayout, child: V, dependency: View): Boolean {
        val appBarLayout = dependency as AppBarLayout

        val maxScroll = appBarLayout.totalScrollRange
        val percentage = Math.abs(appBarLayout.y) / maxScroll.toFloat()
        val scale = 1f - percentage

        if (scale > MIN_SCALE) {
            child.scaleX = scale
            child.scaleY = scale
        } else {
            child.scaleX = MIN_SCALE
            child.scaleY = MIN_SCALE
        }
        child.pivotY = appBarLayout.context.resources.getDimensionPixelSize(R.dimen.detail_pivot_y).toFloat()

        return true
    }
}