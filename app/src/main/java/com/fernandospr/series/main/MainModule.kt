package com.fernandospr.series.main

import com.fernandospr.series.common.repository.SeriesRepository
import dagger.Module
import dagger.Provides

@Module
class MainModule {

    @Provides
    fun provideMainPresenter(seriesRepository: SeriesRepository) = MainPresenter(seriesRepository)

}
