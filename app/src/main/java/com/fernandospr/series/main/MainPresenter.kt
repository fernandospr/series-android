package com.fernandospr.series.main

import com.fernandospr.series.common.BasePresenter
import com.fernandospr.series.common.Main
import com.fernandospr.series.common.TvShow
import com.fernandospr.series.common.repository.RepositoryCallback
import com.fernandospr.series.common.repository.SeriesRepository
import com.fernandospr.series.common.repository.SubscriptionRepositoryListener

class MainPresenter(private val repository: SeriesRepository) :
        BasePresenter<MainPresenter.View>(), SubscriptionRepositoryListener {

    private var main: Main? = null

    override fun attachView(view: View) {
        super.attachView(view)
        repository.addSubscriptionListener(this)
    }

    override fun detachView() {
        repository.removeSubscriptionListener(this)
        super.detachView()
    }

    fun loadMain(savedMain: Main? = null) {
        if (savedMain != null) {
            showMain(savedMain)
        } else {
            view?.showLoading()
            repository.getMain(object : RepositoryCallback<Main> {
                override fun onSuccess(main: Main?) {
                    showMain(main)
                }

                override fun onError() {
                    view?.showError()
                }
            })
        }
    }

    fun loadNextPage() {
        if (main != null && shouldLoadNextPage()) {
            repository.getMain(
                    callback = object : RepositoryCallback<Main> {
                        override fun onSuccess(main: Main?) {
                            updateMainWithNewPages(main)
                        }

                        override fun onError() {
                            view?.showError()
                        }
                    },
                    page = main!!.suggestedShowsPage + 1,
                    previousMain = this@MainPresenter.main
            )
        }
    }

    private fun shouldLoadNextPage() = main!!.suggestedShowsPage < main!!.suggestedShowsTotalPages

    private fun showMain(main: Main?) {
        this.main = main
        if (main == null || main.isEmpty()) {
            view?.showEmptyMain()
        } else {
            view?.showMain(main)
        }
    }

    private fun updateMainWithNewPages(main: Main?) {
        this.main = main
        if (main == null || main.isEmpty()) {
            view?.showEmptyMain()
        } else {
            view?.updateMainWithNewPages(main)
        }
    }

    fun getMain() = main

    override fun onSubscribed(tvShow: TvShow) {
        if (main == null) {
            main = Main(arrayListOf(tvShow), arrayListOf())
        } else {
            main = main!!.subscribe(tvShow)
        }
        showMain(main)
    }

    override fun onUnsubscribed(tvShow: TvShow) {
        if (main == null) {
            main = Main(arrayListOf(), arrayListOf())
        } else {
            main = main!!.unsubscribe(tvShow)
        }
        showMain(main)
    }

    interface View {
        fun showLoading()
        fun showError()
        fun showMain(main: Main)
        fun showEmptyMain()
        fun updateMainWithNewPages(main: Main)
    }

}