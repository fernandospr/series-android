package com.fernandospr.series.main.view

import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.view.ViewCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.fernandospr.series.R
import com.fernandospr.series.common.Main
import com.fernandospr.series.common.SeriesApplication
import com.fernandospr.series.common.TvShow
import com.fernandospr.series.common.view.BaseActivity
import com.fernandospr.series.common.view.EndlessRecyclerViewScrollListener
import com.fernandospr.series.detail.view.DetailActivity
import com.fernandospr.series.main.MainPresenter
import com.fernandospr.series.search.view.SearchActivity
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : BaseActivity(), MainPresenter.View {

    @Inject lateinit var presenter: MainPresenter

    companion object {
        private const val BUNDLE_MAIN = "BUNDLE_MAIN"
    }

    private var linearLayoutManager: LinearLayoutManager? = null
    private var mainAdapter: MainAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        (application as SeriesApplication).component.inject(this)

        setupList()

        presenter.attachView(this)

        val savedMain = savedInstanceState?.getParcelable<Main>(BUNDLE_MAIN)
        presenter.loadMain(savedMain)
    }

    private fun setupList() {
        linearLayoutManager = LinearLayoutManager(this)
        list.layoutManager = linearLayoutManager
        list.addItemDecoration(MainItemDecoration())
        val scrollListener = object : EndlessRecyclerViewScrollListener(linearLayoutManager!!) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                presenter.loadNextPage()
            }
        }
        list.addOnScrollListener(scrollListener)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_search) {
            val intent = SearchActivity.newIntent(this)
            startActivity(intent)
            return true
        }
        return super.onOptionsItemSelected(item)
    }


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        val main = presenter.getMain()
        if (main != null) {
            outState.putParcelable(BUNDLE_MAIN, main)
        }
    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }


    // BaseActivity methods

    override fun getContentContainer(): View = list


    // MainPresenter.View methods

    override fun showLoading() {
        showLoadingContainer()
    }

    override fun showError() {
        showErrorContainer { presenter.loadMain() }
    }

    override fun showMain(main: Main) {
        mainAdapter = MainAdapter(main, object : MainAdapter.Listener {
            override fun onSuggestedClick(view: View, tvShow: TvShow) {
                val intent = DetailActivity.newIntent(this@MainActivity, tvShow)
                startActivity(intent)
            }

            override fun onSubscribedClick(view: View, tvShow: TvShow) {
                val intent = DetailActivity.newIntent(this@MainActivity, tvShow)
                val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this@MainActivity,
                        view,
                        ViewCompat.getTransitionName(view))
                ActivityCompat.startActivity(this@MainActivity, intent, options.toBundle())
            }
        })

        list.adapter = mainAdapter

        showContentContainer()
    }

    override fun updateMainWithNewPages(main: Main) {
        mainAdapter?.update(main)
    }

    override fun showEmptyMain() {
        showNoResultsContainer()
    }
}
