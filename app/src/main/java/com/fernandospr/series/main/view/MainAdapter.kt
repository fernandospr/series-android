package com.fernandospr.series.main.view

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.fernandospr.series.R
import com.fernandospr.series.common.Main
import com.fernandospr.series.common.TvShow
import com.fernandospr.series.common.view.AnimatedRecyclerViewAdapter
import com.fernandospr.series.common.view.inflate
import jp.wasabeef.glide.transformations.GrayscaleTransformation
import kotlinx.android.synthetic.main.header_item.view.*
import kotlinx.android.synthetic.main.subscribed_section_item.view.*
import kotlinx.android.synthetic.main.suggested_item.view.*


class MainAdapter(var main: Main,
                  private val listener: Listener)
    : AnimatedRecyclerViewAdapter<RecyclerView.ViewHolder>() {

    companion object {
        const val TYPE_HEADER = 0
        const val TYPE_SUBSCRIBED_SHOW = 1
        const val TYPE_SUGGESTED_SHOW = 2
    }

    override fun getItemViewType(position: Int): Int {
        if (position == 0) {
            return TYPE_HEADER
        }

        if (main.subscribedShows.isNotEmpty()) {
            if (position == 1) {
                return TYPE_SUBSCRIBED_SHOW
            } else if (position == 2) {
                return TYPE_HEADER
            }
        }
        return TYPE_SUGGESTED_SHOW
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_HEADER -> HeaderViewHolder(parent.inflate(R.layout.header_item))
            TYPE_SUBSCRIBED_SHOW -> SubscribedViewHolder(parent.inflate(R.layout.subscribed_section_item))
            else -> SuggestedViewHolder(parent.inflate(R.layout.suggested_item))
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        if (position == 0) {
            val titleRes = if (main.subscribedShows.isNotEmpty()) {
                R.string.subscribed_series
            } else {
                R.string.suggested_series
            }
            (holder as HeaderViewHolder).bind(holder.itemView.context.getString(titleRes))
            return
        } else if (position == 1 && main.subscribedShows.isNotEmpty()) {
            (holder as SubscribedViewHolder).bind(main.subscribedShows, listener)
            return
        } else if (position == 2 && main.subscribedShows.isNotEmpty()) {
            (holder as HeaderViewHolder).bind(holder.itemView.context.getString(R.string.suggested_series))
            return
        }

        var index = position - 1
        if (main.subscribedShows.isNotEmpty()) {
            index -= 2
        }
        val tvShow = main.suggestedShows[index]
        (holder as SuggestedViewHolder).bind(tvShow, listener)
    }

    override fun getItemCount(): Int {
        var count = 0
        if (main.subscribedShows.isNotEmpty()) {
            count += 2 // header + subscribed row
        }
        if (main.suggestedShows.isNotEmpty()) {
            count += main.suggestedShows.size + 1
        }
        return count
    }

    class HeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(title: CharSequence) = with(itemView) {
            headerText.text = title
        }
    }

    class SubscribedViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(tvShows: List<TvShow>, listener: Listener) = with(itemView) {
            subscribedList.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            if (subscribedList.getItemDecorationAt(0) == null) {
                subscribedList.addItemDecoration(SubscribedItemDecoration())
            }
            subscribedList.adapter = SubscribedAdapter(tvShows, object : SubscribedAdapter.Listener {
                override fun onClick(view: View, tvShow: TvShow) {
                    listener.onSubscribedClick(view, tvShow)
                }
            })
        }
    }

    class SuggestedViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(tvShow: TvShow, listener: Listener) = with(itemView) {
            if (tvShow.genre.isNullOrBlank()) {
                genreContainer.visibility = View.GONE
            } else {
                genreText.text = tvShow.genre?.toUpperCase()
                genreContainer.visibility = View.VISIBLE
            }
            titleText.text = tvShow.title
            Glide.with(context).load(tvShow.backdropUrl)
                    .apply(RequestOptions.bitmapTransform(GrayscaleTransformation()))
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(imageView)
            setOnClickListener {
                listener.onSuggestedClick(itemView, tvShow)
            }
        }
    }

    interface Listener {
        fun onSuggestedClick(view: View, tvShow: TvShow)
        fun onSubscribedClick(view: View, tvShow: TvShow)
    }

    fun update(main: Main) {
        val oldCount = this.itemCount
        this.main = main
        notifyItemInserted(oldCount)
    }
}