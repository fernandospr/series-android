package com.fernandospr.series.main.view

import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View
import com.fernandospr.series.R


class MainItemDecoration : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(outRect: Rect,
                                view: View,
                                parent: RecyclerView,
                                state: RecyclerView.State) {
        val position = parent.getChildAdapterPosition(view)
        val itemViewType = parent.adapter.getItemViewType(position)
        when (itemViewType) {
            MainAdapter.TYPE_HEADER -> {
                outRect.bottom = view.resources.getDimensionPixelSize(R.dimen.header_separator_bottom)
                outRect.top = view.resources.getDimensionPixelSize(R.dimen.header_separator_top)
            }
            MainAdapter.TYPE_SUGGESTED_SHOW -> outRect.bottom = view.resources.getDimensionPixelSize(R.dimen.main_separator_bottom)
            MainAdapter.TYPE_SUBSCRIBED_SHOW -> outRect.bottom = view.resources.getDimensionPixelSize(R.dimen.subscribed_separator_bottom)
        }
        outRect.left = view.resources.getDimensionPixelSize(R.dimen.main_separator_left)
        outRect.right = view.resources.getDimensionPixelSize(R.dimen.main_separator_right)
    }
}