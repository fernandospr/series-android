package com.fernandospr.series.main.view

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.fernandospr.series.R
import com.fernandospr.series.common.TvShow
import com.fernandospr.series.common.view.inflate
import kotlinx.android.synthetic.main.subscribed_item.view.*

class SubscribedAdapter(private val tvShows: List<TvShow>,
                        private val listener: Listener)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return SubscribedItemViewHolder(parent.inflate(R.layout.subscribed_item))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val tvShow = tvShows[position]
        (holder as SubscribedItemViewHolder).bind(tvShow, listener)
    }

    override fun getItemCount() = tvShows.size

    class SubscribedItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(tvShow: TvShow, listener: Listener) = with(itemView) {
            Glide.with(context).load(tvShow.posterUrl)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(imageView)

            setOnClickListener {
                listener.onClick(imageView, tvShow)
            }
        }
    }

    interface Listener {
        fun onClick(view: View, tvShow: TvShow)
    }
}