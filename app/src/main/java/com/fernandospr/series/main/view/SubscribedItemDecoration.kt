package com.fernandospr.series.main.view

import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View
import com.fernandospr.series.R

class SubscribedItemDecoration : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(outRect: Rect,
                                view: View,
                                parent: RecyclerView,
                                state: RecyclerView.State) {
        outRect.right = view.resources.getDimensionPixelSize(R.dimen.main_separator_right)
    }
}