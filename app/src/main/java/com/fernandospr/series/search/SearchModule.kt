package com.fernandospr.series.search

import com.fernandospr.series.common.repository.SeriesRepository
import dagger.Module
import dagger.Provides

@Module
class SearchModule {

    @Provides
    fun provideSearchPresenter(seriesRepository: SeriesRepository) = SearchPresenter(seriesRepository)

}
