package com.fernandospr.series.search

import com.fernandospr.series.common.BasePresenter
import com.fernandospr.series.common.TvShow
import com.fernandospr.series.common.repository.RepositoryCallback
import com.fernandospr.series.common.repository.SeriesRepository
import com.fernandospr.series.common.repository.SubscriptionRepositoryListener

class SearchPresenter(private val repository: SeriesRepository) :
        BasePresenter<SearchPresenter.View>(), SubscriptionRepositoryListener {

    private var tvShows: List<TvShow>? = null

    override fun attachView(view: View) {
        super.attachView(view)
        repository.addSubscriptionListener(this)
    }

    override fun detachView() {
        repository.removeSubscriptionListener(this)
        super.detachView()
    }

    fun restoreSavedSearchResults(savedResults: List<TvShow>?, query: String) {
        if (savedResults != null) {
            showResults(savedResults, query)
        }
    }

    fun search(query: String) {
        view?.showLoading()
        repository.search(query = query,
                callback = object : RepositoryCallback<List<TvShow>> {
            override fun onSuccess(tvShows: List<TvShow>?) {
                showResults(tvShows, query)
            }

            override fun onError() {
                view?.showError()
            }
        })
    }

    private fun showResults(tvShows: List<TvShow>?, query: String) {
        this@SearchPresenter.tvShows = tvShows
        if (tvShows != null && tvShows.isNotEmpty()) {
            view?.showSearchResults(tvShows)
        } else {
            view?.showEmptySearchResults(query)
        }
    }

    fun toggleSubscription(tvShow: TvShow) {
        if (tvShow.subscribed) {
            repository.unsubscribe(tvShow)
        } else {
            repository.subscribe(tvShow)
        }
    }

    fun getSearchResults() = tvShows

    override fun onSubscribed(tvShow: TvShow) {
        if (tvShows != null) {
            val existingTvShow = tvShows!!.find { it.id == tvShow.id }
            if (existingTvShow != null) {
                val index = tvShows!!.indexOf(existingTvShow)
                existingTvShow.subscribed = true
                view?.refreshSubscriptionStatusAtIndex(index)
            }
        }
    }

    override fun onUnsubscribed(tvShow: TvShow) {
        if (tvShows != null) {
            val existingTvShow = tvShows!!.find { it.id == tvShow.id }
            if (existingTvShow != null) {
                val index = tvShows!!.indexOf(existingTvShow)
                existingTvShow.subscribed = false
                view?.refreshSubscriptionStatusAtIndex(index)
            }
        }
    }

    interface View {
        fun showLoading()
        fun showError()
        fun showSearchResults(tvShows: List<TvShow>)
        fun showEmptySearchResults(query: String)
        fun refreshSubscriptionStatusAtIndex(index: Int)
    }

}