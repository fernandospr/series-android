package com.fernandospr.series.search.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.view.ViewCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.View
import com.fernandospr.series.R
import com.fernandospr.series.common.SeriesApplication
import com.fernandospr.series.common.TvShow
import com.fernandospr.series.common.view.BaseActivity
import com.fernandospr.series.detail.view.DetailActivity
import com.fernandospr.series.search.SearchPresenter
import kotlinx.android.synthetic.main.activity_search.*
import javax.inject.Inject


class SearchActivity : BaseActivity(), SearchPresenter.View {

    @Inject lateinit var presenter: SearchPresenter

    companion object {

        private const val BUNDLE_RESULTS = "BUNDLE_RESULTS"

        fun newIntent(context: Context): Intent {
            val intent = Intent(context, SearchActivity::class.java)
            return intent
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        (application as SeriesApplication).component.inject(this)

        list.layoutManager = LinearLayoutManager(this)
        setupSearchView()
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        presenter.attachView(this)

        val savedResults = savedInstanceState?.getParcelableArrayList<TvShow>(BUNDLE_RESULTS)
        presenter.restoreSavedSearchResults(savedResults, searchView.query.toString())
    }

    override fun onSupportNavigateUp(): Boolean {
        ActivityCompat.finishAfterTransition(this)
        return true
    }

    private fun setupSearchView() {
        searchView.queryHint = getString(R.string.action_search)
        searchView.isFocusable = true
        searchView.setIconifiedByDefault(false)
        searchView.requestFocusFromTouch()
        searchView.requestFocus()
        searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
            override fun onQueryTextChange(newText: String?) = false

            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query != null) {
                    presenter.search(query)
                }
                searchView.clearFocus()
                return true
            }
        })
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        val searchResults = presenter.getSearchResults()
        if (searchResults != null) {
            val array = arrayListOf<TvShow>()
            array.addAll(searchResults)
            outState.putParcelableArrayList(BUNDLE_RESULTS, array)
        }
    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }


    // BaseActivity methods

    override fun getContentContainer(): View = list


    // SearchPresenter.View methods

    override fun showLoading() {
        showLoadingContainer()
    }

    override fun showError() {
        showErrorContainer { presenter.search(searchView.query.toString()) }
    }

    override fun showSearchResults(tvShows: List<TvShow>) {
        list.adapter = SearchAdapter(tvShows, object : SearchAdapter.Listener {

            override fun onClick(view: View, tvShow: TvShow) {
                val intent = DetailActivity.newIntent(this@SearchActivity, tvShow)
                val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this@SearchActivity,
                        view,
                        ViewCompat.getTransitionName(view))
                ActivityCompat.startActivity(this@SearchActivity, intent, options.toBundle())
            }

            override fun onToggleSubscription(tvShow: TvShow) {
                presenter.toggleSubscription(tvShow)
            }
        })
        showContentContainer()
    }

    override fun showEmptySearchResults(query: String) {
        showNoResultsContainer(getString(R.string.search_noresults, query))
    }

    override fun refreshSubscriptionStatusAtIndex(index: Int) {
        list.adapter.notifyItemChanged(index)
    }
}