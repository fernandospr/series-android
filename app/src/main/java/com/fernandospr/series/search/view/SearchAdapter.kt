package com.fernandospr.series.search.view

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.fernandospr.series.R
import com.fernandospr.series.common.TvShow
import com.fernandospr.series.common.view.inflate
import kotlinx.android.synthetic.main.search_item.view.*

class SearchAdapter(private val tvShows: List<TvShow>,
                    private val listener: Listener)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return SearchViewHolder(parent.inflate(R.layout.search_item))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val tvShow = tvShows[position]
        (holder as SearchViewHolder).bind(tvShow, listener)
    }

    override fun getItemCount() = tvShows.size

    class SearchViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(tvShow: TvShow, listener: Listener) = with(itemView) {
            if (tvShow.genre.isNullOrBlank()) {
                genreText.visibility = View.GONE
            } else {
                genreText.text = tvShow.genre?.toUpperCase()
                genreText.visibility = View.VISIBLE
            }
            titleText.text = tvShow.title
            Glide.with(context).load(tvShow.posterUrl)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(imageView)
            setOnClickListener {
                listener.onClick(imageView, tvShow)
            }
            setupSubscribeButton(subscribeButton, tvShow, listener)
        }

        private fun setupSubscribeButton(subscribeButton: TextView,
                                         tvShow: TvShow,
                                         listener: Listener) {
            with(subscribeButton) {
                if (tvShow.subscribed) {
                    setTextColor(ContextCompat.getColor(context, android.R.color.black))
                    text = context.getString(R.string.subscribed)
                    setBackgroundResource(R.drawable.bg_subscribed_search)
                    alpha = 0.4f
                } else {
                    setTextColor(ContextCompat.getColor(context, android.R.color.white))
                    text = context.getString(R.string.subscribe)
                    setBackgroundResource(R.drawable.bg_subscribe_search)
                    alpha = 0.23f
                }
                setOnClickListener {
                    listener.onToggleSubscription(tvShow)
                }
            }
        }
    }

    interface Listener {
        fun onClick(view: View, tvShow: TvShow)
        fun onToggleSubscription(tvShow: TvShow)
    }
}